import json
from flask import Flask, request, make_response, jsonify
from create_response_data import Response
from intents import mall_opening_timing_2, mall_opening_timing_3, mall_opening_timing_4, \
    mall_opening_timing_5, mall_opening_timing_6,store_search,category_search,tag_search, parking, fountain
from create_request_data import Request
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"
mongo = PyMongo(app)
log = app.logger


@app.route('/', methods=['POST'])
def webhook():
    """This method handles the http requests for the Dialogflow webhook

    This is meant to be used in conjunction with the weather Dialogflow agent

    for every intent create an object class inside the intents folder
    """
    req = Request(request.get_json(silent=True, force=True))

    try:
        intent, follow_up_intent = req.get_intent()
        print("intent", intent, follow_up_intent)
    except AttributeError:
        return 'json error'
    try:
        if intent == 'Mall Open Timing 2':
            res = mall_opening_timing_2.TimingIntent(req, follow_up_intent).get_response_data()
        elif intent == 'Mall Open Timing 3':
            res = mall_opening_timing_3.TimingIntent(req, follow_up_intent).get_response_data()
        elif intent == 'Mall Open Timing 4':
            res = mall_opening_timing_4.TimingIntent(req, follow_up_intent).get_response_data()
        elif intent == 'Mall Open Timing 5':
            res = mall_opening_timing_5.TimingIntent(req, follow_up_intent).get_response_data()
        elif intent == 'Mall Open Timing 6':
            res = mall_opening_timing_6.TimingIntent(req, follow_up_intent).get_response_data()
        elif intent == 'Store Search':
            res = store_search.StoreSearch(req, follow_up_intent).get_response_data()
        elif intent == 'Category Search':
            res = category_search.CategorySearch(req, follow_up_intent).get_response_data()
        elif intent == 'Tags Search':
            res = tag_search.TagSearch(req, follow_up_intent).get_response_data()

        elif intent == '03 Malls - Parking - What is the nearest parking to store':
            res = parking.NearestParking(req, follow_up_intent).get_response_data()

        elif intent == '03 Malls - Parking - Where is the best place to park in The Dubai Mall':
            res = parking.ParkingAvailability(req, follow_up_intent).get_response_data()

        elif intent == '03 Malls - Fountain - How often is the Dubai Fountain show':
            res = fountain.FountainTiming(req, follow_up_intent).get_response_data()

        elif intent == '03 Malls - Lights Show - How often is the Burj Khalifa Light Show':
            res = fountain.LightShowTiming(req, follow_up_intent).get_response_data()
        else:
            res = Response("Didn't get what you want to say", "ACTIONS_ON_GOOGLE",req.session)
            res.add_simple_response(["Can You Say It Again"])
    except Exception as Err:
        print (Err)
        res = Response("Didn't get what you want to say", "ACTIONS_ON_GOOGLE",req.session)
        res.add_simple_response(["Can You Say It Again"])

    # print(make_response(jsonify(res)))

    return make_response(jsonify(res))

@app.route('/test', methods=['GET'])
def testwebhook():
    return make_response(jsonify({"status":"success"}))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
