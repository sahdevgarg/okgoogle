import requests
import pymongo
import json
from datetime import datetime
IS_DEV = False

if IS_DEV:
   proxies = {
      "http": "http://sahdev.garg:sA%409873106841@isaserver.emaarmgf.com:8080",
      "https": "http://sahdev.garg:sA%409873106841@isaserver.emaarmgf.com:8080",
   }
else:
   proxies = {
      "http": "http://isaserver.emaarmgf.com:8080",
      "https": "http://isaserver.emaarmgf.com:8080",
   }

# Create the session and set the proxies.
s = requests.Session()
s.proxies = proxies

# Make the HTTP request through the session.
r = s.get("http://91.72.205.8:8080/indect/structure")


client = pymongo.MongoClient()
db = client.tdm
collection = db.car_parkings
parking_data = json.loads(r.text)
parking_data_store = []

for zone in parking_data["Zones"]:
   for parking in zone["Zones"]:
      if zone["Name"] == "Grand Parking":
         parking["zone"] = "Grand Parking"
         parking["parking"] = parking["Name"].replace("Level ","P") + ", Grand Parking"
      elif zone["Name"] == "Fashion Parking":
         parking["zone"] = "Fashion Parking"
         parking["parking"] = parking["Name"].replace("Fashion Level ", "P") + ", Fashion Parking"

      parking["available"] = parking["TotalBays"] - parking["OccupiedBays"]
      parking_data_store.append(parking)

collection.insert_one({"time":int(datetime.now().strftime('%s')),"parking":parking_data_store})