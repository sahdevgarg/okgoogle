from conf.intent_objects import *
import copy


class Request(object):
    def __init__(self, req):
        self.req = req
        self.queryResult = self.req.get("queryResult")
        self.output_context = self.queryResult["outputContexts"] if "outputContexts" in self.queryResult else []
        self.session = self.req["session"]

    def get_intent(self):
        intent_text = self.queryResult["intent"]['displayName'].split(":")
        intent = intent_text[0].strip(" ")
        follow_up_intent = ""
        if len(intent_text) > 1:
            follow_up_intent = intent_text[1].strip(" ")
        return intent,follow_up_intent

    @property
    def get_parameters(self):
        return self.queryResult["parameters"] if "parameters" in self.queryResult else {}

    def check_context(self,name):
        for output_context in self.output_context:
            if name == output_context["name"].split("/")[-1]:
                return True

        return False

    def get_context_parameters(self,name):
        for output_context in self.output_context:
            if name == output_context["name"].split("/")[-1]:
                return output_context["parameters"]

        return {}

