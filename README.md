# okgoogle

# SetUp
  - Take clone of the project
  - Use python3 to create the virtual environment
  - setup the virtual environment
  - run command pip install -r requirement.txt from the project directory

# RunServer

  - Activate the virtual env
  - run python app.py
  - Also use ngrok to get a global domain name so that dialog flow can use it.

# DialogFlow app
  - Create an app on dialogflow and define intents.
  - If you want that your intent response will come from server the active the enable webhook call in the intent's webhook.
  - Also define the ngrok domain in the fulfillment webhook (make sure it is https).

# Backend
- for every intent create a seperate python class inside the in intent folder in the code.
- Write all the bussiness logic there only.
- Currently the reponse is only supporting simple response, basic card and suggestion.
- More will be coming soon..
