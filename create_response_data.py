from conf.intent_objects import *
import copy


class Response(object):
    def __init__(self, fulfillment_text,platform="",session = ""):
        self.response_json = {"fulfillmentText": fulfillment_text, "fulfillmentMessages": []}
        self.platform = platform
        self.session = session

    def add_suggestions(self, suggestions_list):
        detail = {"suggestions": copy.deepcopy(SUGGESTIONS_OBJ), "platform": self.platform}
        for suggestion in suggestions_list:
            detail["suggestions"]["suggestions"].append({"title": suggestion})
        self.response_json["fulfillmentMessages"].append(detail)

    def add_card(self, card_data):
        detail = {"basicCard": Card(card_data).card, "platform": self.platform}
        self.response_json["fulfillmentMessages"].append(detail)

    def add_simple_response(self, text_to_speech_list):
        detail = {"simpleResponses": copy.deepcopy(SIMPLE_RESPONSE_OBJ), "platform": self.platform}
        for text_to_speech in text_to_speech_list:
            detail["simpleResponses"]["simpleResponses"].append({"textToSpeech": text_to_speech})
        self.response_json["fulfillmentMessages"].append(detail)

    def add_output_context(self,name,lifespanCount=5,parameters={}):
        context_obj = copy.deepcopy(CONTEXT_OBJ)
        context_obj["name"] = self.session+"/contexts/"+name
        context_obj["lifespanCount"] = lifespanCount
        context_obj["parameters"] = parameters
        if "outputContexts" not in self.response_json:
            self.response_json.update({"outputContexts":[]})
        self.response_json["outputContexts"].append(context_obj)


class Card(object):
    def __init__(self, data):
        self.card = self.add_card(data)

    def add_card(self, data):
        detail = copy.deepcopy(BASIC_CARD_OBJ)
        if "title" not in data:
            raise Exception("Title is missing")
        else:
            detail["title"] = data["title"]

        if "subtitle" not in data:
            raise Exception("Subtitle is missing")
        else:
            detail["subtitle"] = data["subtitle"]

        if "formattedText" not in data:
            raise Exception("Formatted Text is missing")
        else:
            detail["formattedText"] = data["formattedText"]

        if "imageUri" not in data:
            raise Exception("Image Uri is missing")
        else:
            detail["image"]["imageUri"] = data["imageUri"]

        if "accessibilityText" not in data:
            raise Exception("accessibilityText is missing")
        else:
            detail["image"]["accessibilityText"] = data["accessibilityText"]

        if "buttons" in data and not isinstance(data["buttons"], (list,)):
            raise Exception("buttons should be a list")
        else:
            for button in data["buttons"]:
                detail["buttons"].append(self.add_card_buttons(button))
        return detail

    @staticmethod
    def add_card_buttons(button):
        detail = copy.deepcopy(CARD_BUTTON_OBJ)
        if "title" not in button:
            raise Exception("Button Title is missing")
        else:
            detail["title"] = button["title"]

        if "button_uri" not in button:
            raise Exception("Button Uri is missing")
        else:
            detail["openUriAction"]["uri"] = button["button_uri"]
        return detail
