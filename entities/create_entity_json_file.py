import json

class CreateEntityFile(object):
    def __init__(self,data,entity_name,auto_expansion=False):
        self.data = data
        self.entity_name = entity_name
        self.auto_expansion = auto_expansion
        self.create_file()

    def create_file(self):
        detail = {"id":"","name":self.entity_name,"isOverridable":True,"isEnum":False,
                  "automatedExpansion":self.auto_expansion,"entries":self.data}

        with open("entities/"+self.entity_name+'.json', 'w') as outfile:
            json.dump(detail, outfile)
