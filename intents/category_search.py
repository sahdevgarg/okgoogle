from create_response_data import Response
import json
import pymongo

from datetime import datetime, time
from utility import *

Opening_time = time(10, 00)
Closing_time = time(23, 59)

# Original test case from OP
myclient = pymongo.MongoClient("mongodb://localhost:27017/")


class CategorySearch(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.collection = myclient["tdm"]["stores"]
        self.parameters = self.req.get_parameters

    def get_response_data(self):
        if not self.follow_up_intent:
            floor = self.parameters["floors"] if "floors" in self.parameters else ""
            category = self.parameters["categories"] if "categories" in self.parameters else ""
            if category and floor:
                text = self.get_store_on_floor_with_category(category, floor)
            if category and not floor:
                text, flag = self.get_store_with_category(category)
            res = Response(text, "ACTIONS_ON_GOOGLE",self.req.session)
            res.add_simple_response([text])
        else:
            # print("i am here...")
            res = self.get_response_for_followup()

        return res.response_json

    def get_store_on_floor_with_category(self, category, floor):
        data = list(self.collection.find({"categories": {"$in": [category]}, "floor": floor}))
        if data and len(data) == 1:

            if data[0]["parking"]:
                text = "Oh Yes, there is a %s on %s of dubai mall under the name %s near %s" % (category, data[0]["floor"],
                                                                                   data[0]["store_name"],
                                                                                   data[0]["parking"])
            else:
                text = "Oh Yes, there is a %s on %s of dubai mall under the name %s" % (
                category, data[0]["floor"],
                data[0]["store_name"])
        elif data and len(data) == 2:
            store_data = "The First one is "
            if data[0]["parking"] != "":
                store_data += "%s near %s." % (data[0]["store_name"], data[0]["parking"])
            else:
                store_data += "%s." % (data[0]["store_name"])
            store_data += " and the second one is "
            if data[1]["parking"]:
                store_data += "%s near %s." % (data[1]["store_name"], data[1]["parking"])
            else:
                store_data += "%s." % (data[1]["store_name"])
            text = "There are %s %s stores on the %s of Dubai Mall. We recommend %s " % (len(data), category, floor, store_data)

        elif data and len(data) > 1:
            if data[0]["parking"]:
                store_data = "%s near %s." % (data[0]["store_name"], data[0]["parking"])
            else:
                store_data = "%s." % (data[0]["store_name"])
            text = "There are %s %s stores on the %s of Dubai Mall. We recommend %s " % (len(data), category, floor, store_data)
        else:
            text, flag = self.get_store_with_category(category)
            text = text if flag else "Sorry there isn't a %s store on the %s , but we do have one on %s" % (category, floor, text)
        return text

    def get_store_with_category(self, category):
        # print("finding with category 1")
        data = list(self.collection.find({"categories": {"$in": [category]}}))
        # print("finding with category 2")
        text = self.get_text(data,category)
        flag = False
        # print("finding with category 3")
        if text == "":
            data = list(self.collection.find({"tags": {"$in": [category+" "]}}))
            text = self.get_text(data, category)
            if text == "":
                text = "No store is available in Dubai Mall for %s, but it is available on Noon!" % (category)
                flag = True

        # print(text)
        return text, flag


    @staticmethod
    def get_text(data,category):
        # print("get_text 1")
        if data and len(data) == 1:
            # print("get_text 2")
            text = "There is a %s in Dubai Mall under the name %s on %s near %s" % (
                category, data[0]["store_name"], data[0]["floor"],
                data[0]["parking"])
        elif data and len(data) == 2:
            # print("get_text 3")
            store_data = "The First one is "
            if data[0]["parking"] != "":
                store_data += "%s on %s near %s." % (data[0]["store_name"],data[0]["floor"], data[0]["parking"])
            else:
                store_data += "%s on %s" % (data[0]["store_name"],data[0]["floor"])
            store_data += " and the second one is "
            if data[1]["parking"]:
                store_data += "%s on %s near %s." % (data[1]["store_name"],data[1]["floor"], data[1]["parking"])
            else:
                store_data += "%s on %s" % (data[1]["store_name"],data[1]["floor"])

            # print("get_text 4")
            text = "There are %s %s stores in Dubai Mall. %s" % (
                len(data), category, store_data)
        elif data and len(data) > 1:
            print("get_text 5")
            store_data = ""
            # print(data)
            # for data_obj in data:
            #     print(data_obj["store_name"].encode("utf-8")+" ::: "+ data_obj["floor"]+" ::: "+ data_obj["parking"])
            #     store_data += " %s on %s near %s " % (
            #         data_obj["store_name"].encode("utf-8"), data_obj["floor"], data_obj["parking"])

            # print("get_text 6")
            store_data = " %s on %s " % (data[0]["store_name"], data[0]["floor"])
            if data[0]["parking"]:
                store_data += " near %s , " % (data[0]["parking"])

            if len(data) > 1:
                store_data1 = " %s on %s " % (data[1]["store_name"], data[1]["floor"])
                if data[1]["parking"]:
                    store_data1 += " near %s , " % (data[1]["parking"])

            text = "There are %s %s stores in Dubai Mall. %s" % (len(data), category, store_data)

            # print("get_text 7")
            if len(data) == 2:
                text += " and %s" % (store_data1)
            elif len(data) > 2:
                text += " and %s more" % (len(data)-1)

        else:
            text = ""

        # print("get_text 8")
        return text
