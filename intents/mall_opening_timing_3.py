from create_response_data import Response
import json

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)
# Original test case from OP


class TimingIntent(object):

    def __init__(self,req,follow_up_intent = None):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")

    def get_response_data(self):
        if not self.follow_up_intent:
            date = get_date_from_weekday(self.req.get_parameters["weekday"])
            is_holiday, holiday = check_for_holiday(date)
            if is_holiday:
                text = "Mall is closed on this %s because of %s" % (self.req.get_parameters["weekday"],holiday)
            else:
                if "weekday" in self.req.get_parameters:
                    text = "The Dubai Mall is open from 10am to 12 midnight on "+self.req.get_parameters["weekday"]

            res = Response(text, "ACTIONS_ON_GOOGLE")
            res.add_simple_response([text])

        else:
            res = self.get_response_for_followup()

        return res.response_json

    def get_response_for_followup(self):
        if self.follow_up_intent == "followup1":
            date = get_date_from_weekday(self.req.get_parameters["weekday"])
            if self.req.get_parameters["nextprevious"] == "next":
                date = date + timedelta(days=7)
            elif self.req.get_parameters["nextprevious"] == "last":
                date = date + timedelta(days=-7)
            is_holiday, holiday = check_for_holiday(date)
            if is_holiday:
                text = "Mall is closed on %s %s because of %s" % (self.req.get_parameters["nextprevious"],self.req.get_parameters["weekday"], holiday)
            else:
                if "weekday" in self.req.get_parameters:
                    text = "The Dubai Mall is open from 10am to 12 midnight on %s %s"%(self.req.get_parameters["nextprevious"],self.req.get_parameters["weekday"])

            res = Response(text, "ACTIONS_ON_GOOGLE")
            res.add_simple_response([text])
        return res
