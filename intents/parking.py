from create_response_data import Response
import json
import pymongo

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)
from bson import ObjectId

myclient = pymongo.MongoClient("mongodb://localhost:27017/")


class NearestParking(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.collection_stores = myclient["tdm"]["stores"]
        self.collection_parking = myclient["tdm"]["car_parkings"]
        self.parameters = self.req.get_parameters
        self.store_id = ""

    def get_response_data(self):

        floor = self.parameters["floors"] if "floors" in self.parameters else ""
        store = self.parameters["stores"] if "stores" in self.parameters else ""

        store_data = []
        text = ""

        if store:
            store_data = list(self.collection_stores.find({"store_name": store}))
        elif store and floor:
            store_data = list(self.collection_stores.find({"store_name": store, "floor": floor}))

        if store_data and store_data[0]["parking"]:
            parking = list(self.collection_parking.find().sort('time', -1))[0]
            preferred_parking = [p for p in parking["parking"] if p["parking"] == store_data[0]["parking"]]
            if preferred_parking:
                preferred_parking = preferred_parking[0]
                if preferred_parking["available"] > 0:
                    text = "%s is the nearest place to park for %s at the Dubai Mall. " \
                           "Currently, there are %s free spots available!" % (preferred_parking["parking"], store, preferred_parking["available"])
                else:
                    text = "%s is the nearest place to park for %s at the Dubai Mall. " % (preferred_parking["parking"], store)
            else:
                parking = list(self.collection_parking.find().sort('time', -1))[0]
                preferred_parking = max(parking["parking"], key=lambda k: k['available'])
                text = "%s is the nearest place to park for %s at the Dubai Mall. " \
                       "Currently, there are %s free spots available!" % (
                    preferred_parking["parking"], store, preferred_parking["available"])

        res = Response(text, "ACTIONS_ON_GOOGLE", self.req.session)

        return res.response_json


class ParkingAvailability(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.collection_parking = myclient["tdm"]["car_parkings"]

    def get_response_data(self):
        parking = list(self.collection_parking.find().sort('time', -1))[0]
        preferred_parking = max(parking["parking"], key=lambda k: k['available'])
        res = Response("%s is the best place to park. It has %s free parking spots at the moment."
                       % (preferred_parking["parking"], preferred_parking["available"]), "ACTIONS_ON_GOOGLE", self.req.session)

        return res.response_json
