from create_response_data import Response
import json
import pymongo

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)

# Original test case from OP
myclient = pymongo.MongoClient("mongodb://localhost:27017/")


class TagSearch(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.collection = myclient["tdm"]["stores"]
        self.parameters = self.req.get_parameters
        self.recommended_store = {}

    def get_response_data(self):
        if not self.follow_up_intent:
            tag = self.parameters["tags"] if "tags" in self.parameters else ""
            floor = self.parameters["floors"] if "floors" in self.parameters else ""
            if tag and floor:
                text = self.get_store_on_floor_with_tag()
            else:
                text = self.get_store_with_tag(tag)
            res = Response(text, "ACTIONS_ON_GOOGLE",self.req.session)
            res.add_simple_response([text])
            # res.add_simple_response([" Should I send you an email with the list of stores?"])
            if self.recommended_store:
                res.add_output_context("recommended_store", 4,
                                   {"store_name": self.recommended_store["store_name"], "floor": floor,
                                    "store_id": str(self.recommended_store["_id"])})
        else:
            res = self.get_response_for_followup()

        # print(res.response_json)

        return res.response_json

    def get_store_on_floor_with_tag(self, tag, floor):
        data = list(self.collection.find({"tag": {"$in": [tag]}, "floor": floor}))
        if data and len(data) == 1:
            text = "%s on %s near %s offers %s" % (
                data[0]["store_name"], data[0]["floor"],
                data[0]["parking"], tag)
            self.recommended_store = data[0]
        elif data and len(data) == 2:
            text = "There are %s stores in The Dubai Mall on %s which offers %s. " \
                   "First is %s on %s and the second is %s on %s." % (
                len(data),floor, tag, data[0]["store_name"], data[0]["floor"], data[1]["store_name"], data[1]["floor"])
        elif data and len(data) > 2:
            text = "There are %s stores in The Dubai Mall on %s which offers %s. We would recommend %s on %s." % (
                len(data),floor, tag, data[0]["store_name"], data[0]["floor"])
            self.recommended_store = data[0]
        else:
            text = "No store is available in Dubai Mall for %s, but it is available on Noon!" % (tag)
        return text

    def get_store_with_tag(self, tag):
        data = list(self.collection.find({"tags": {"$in": [tag]}}))
        text = self.get_text(data,tag)
        return text

    def get_text(self,data,tag):
        if data and len(data) == 1:
            text = "%s on %s near %s offers %s" % (
                 data[0]["store_name"], data[0]["floor"],
                data[0]["parking"],tag)
            self.recommended_store = data[0]
        elif data and len(data) == 2:
            text = "There are %s stores in The Dubai Mall which offers %s. First is %s on %s and the second is %s on %s" % (
                len(data), tag, data[0]["store_name"],data[0]["floor"],data[1]["store_name"],data[1]["floor"])
        elif data and len(data) > 2:
            text = "There are %s stores in The Dubai Mall which offers %s. We would recommend %s on %s" % (
                len(data), tag, data[0]["store_name"], data[0]["floor"])
            self.recommended_store = data[0]
        else:
            text = "No store is available in Dubai Mall for %s, but it is available on Noon!" % (tag)
        return text
