
from create_response_data import Response
import json
import pymongo

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)
from bson import ObjectId
myclient = pymongo.MongoClient("mongodb://localhost:27017/")


class FountainTiming(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.parameters = self.req.get_parameters
        self.store_id = ""

    def get_response_data(self):

        time_diff = get_time_diff()
        next_show= ""

        if time_diff[1]!=0:
            next_show += str(time_diff[1]) + " hours "

        if time_diff[2] != 0:
            next_show += str(time_diff[2]) + " minutes"

        text = "The Dubai Fountain serenades young and old alike with an acrobatic display of water and " \
               "dazzling music to match. The Dubai Fountain has shows every 30 minutes, starting from 6pm to 11pm. " \
               "The next show will be at %s, which is in  %s." % (time_diff[0], next_show)

        res = Response(text, "ACTIONS_ON_GOOGLE", self.req.session)

        return res.response_json


class LightShowTiming(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.parameters = self.req.get_parameters
        self.store_id = ""

    def get_response_data(self):

        time_diff = get_time_diff()
        next_show= ""

        if time_diff[1]!=0:
            next_show += str(time_diff[1]) + " hours "

        if time_diff[2] != 0:
            if len(next_show) > 0:
                next_show += " and "
            next_show += str(time_diff[2]) + " minutes"

        if time_diff[2] == 0 and time_diff[1] == 0:
            next_show = "few moments"

        text = "The Burj Khalifa Lights Show features amazing imagery celebrating the best Dubai and beyond. " \
               "It happens every 30 minutes, starting from 6pm to 11pm. The next show will be at %s, " \
               "which is in %s." % (time_diff[0], next_show)

        res = Response(text, "ACTIONS_ON_GOOGLE", self.req.session)

        return res.response_json


def get_time_diff():
    from datetime import datetime, timedelta
    import pytz

    tz = pytz.timezone('Asia/Dubai')

    t = datetime.now(tz)
    if t.hour < 18:
        when_time = "6 pm"
        hour_diff = 18 - t.hour - 1
        min_diff = 60 - t.minute
    elif t.hour > 23:
        hour_diff = 23 - t.hour
        min_diff = 60 - t.minute
    else:
        if t.minute > 30:
            when_time = str(t.hour+1-12)+" pm"
            hour_diff = 0
            min_diff = 60 - t.minute
        else:
            when_time = str(t.hour-12) + ":30 pm"
            hour_diff = 0
            min_diff = 30 - t.minute

    print([when_time, hour_diff, min_diff])
    return [when_time, hour_diff, min_diff]


# get_time_diff()