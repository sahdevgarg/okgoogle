from create_response_data import Response
import json

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)


# Original test case from OP


class TimingIntent(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")

    def get_response_data(self):
        if not self.follow_up_intent:
            date = get_date_from_day(self.req.get_parameters["day"])
            openclose = self.req.get_parameters["openclose"]
            is_holiday, holiday = check_for_holiday(date)
            if is_holiday and self.req.get_parameters["day"] == "today":
                text = "Mall is closed today because of %s" % (holiday)
            elif is_holiday and self.req.get_parameters["day"] != "today":
                text = "Mall will be  closed on %s because of %s" % (self.req.get_parameters["day"], holiday)
            else:
                if "day" in self.req.get_parameters:
                    if self.req.get_parameters["day"] in ["today", "now"]:
                        current_time = datetime.now().time()
                        if is_time_between(Opening_time, Closing_time, current_time):
                            text_str = "it is open %s and closes in %s hours at midnight" % (self.req.get_parameters["day"],
                                                                                        str(time_diff(current_time,
                                                                                                      Closing_time)))
                            text = "%s %s" % (
                                "Yes" if openclose == "open" else "No", text_str)
                        else:
                            text_str = "it is closed %s and opens in %s hours at 10am" % (
                            self.req.get_parameters["day"],
                            str(time_diff(Opening_time, current_time)))
                            text = "%s %s" % ("No" if openclose == "open" else "Yes", text_str)
                    else:
                        if openclose == "open":
                            text = "The Dubai Mall opens at 10:00 am " + self.req.get_parameters["day"]
                        else:
                            text = "The Dubai Mall closes at midnight " + self.req.get_parameters["day"]

            res = Response(text, "ACTIONS_ON_GOOGLE")
            res.add_simple_response([text])
        else:
            res = self.get_response_for_followup()

        return res.response_json

    def get_response_for_followup(self):
        if self.follow_up_intent == "followup1":
            date = get_date_from_day(self.req.get_parameters["day"])
            is_holiday, holiday = check_for_holiday(date)
            if is_holiday and self.req.get_parameters["day"] == "today":
                text = "No, Mall is closed today because of %s" % (holiday)
            elif is_holiday and self.req.get_parameters["day"] != "today":
                text = "No, Mall will be closed %s because of %s" % (self.req.get_parameters["day"], holiday)
            else:
                if "day" in self.req.get_parameters:
                    if self.req.get_parameters["day"] != "today":
                        text = "Yes , It will open at 10:00 am " + self.req.get_parameters["day"]

                    else:
                        current_time = datetime.now().time()
                        if is_time_between(Opening_time, Closing_time, current_time):
                            text = "Yes, It's open now and closes in %s hours at midnight" % (
                                str(time_diff(current_time, Closing_time)))
                        else:
                            text = "Yes, It's closed now and opens in %s hours at 10am" % (
                                str(time_diff(Opening_time, current_time)))

            res = Response(text, "ACTIONS_ON_GOOGLE")
            res.add_simple_response([text])
        return res
