from create_response_data import Response
import json

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)
# Original test case from OP


class TimingIntent(object):

    def __init__(self,req,follow_up_intent = None):
        self.req = req

    def get_response_data(self):
        holiday_name = self.req.get_parameters["holiday"]
        is_holiday, holiday = check_for_holiday(get_holiday_date(holiday_name))
        openclose = self.req.get_parameters["openclose"]
        if holiday:
            if is_holiday and openclose == "open":
                text = "No Mall is closed on %s " % (holiday_name)
            if is_holiday and openclose == "close":
                text = "Yes Mall is closed on %s " % (holiday_name)
            if not is_holiday:
                text = "Mall is open on %s " % (holiday_name)
        else:
            text = "Mall is open on %s"%(holiday_name)

        res = Response(text, "ACTIONS_ON_GOOGLE")
        res.add_simple_response([text])

        return res.response_json