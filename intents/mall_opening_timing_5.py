from create_response_data import Response
import json

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)
# Original test case from OP


class TimingIntent(object):

    def __init__(self,req,follow_up_intent = None):
        self.req = req

    def get_response_data(self):
        date_param = self.req.get_parameters["date"]
        date = datetime.strptime(date_param.split("+")[0], "%Y-%m-%dT%H:%M:%S")
        is_holiday, holiday = check_for_holiday(date.date())
        if is_holiday:
            text = "Mall is closed on %s because of %s" % (datetime.strftime(date,"%d %B"),holiday)
        else:
            if "date" in self.req.get_parameters:
                text = "The Dubai Mall is open from 10am to 12 midnight on "+datetime.strftime(date,"%d %B")

        res = Response(text, "ACTIONS_ON_GOOGLE")
        res.add_simple_response([text])

        return res.response_json