from create_response_data import Response
import json
import pymongo

from datetime import datetime, time
from utility import *

Opening_time = time(6, 00)
Closing_time = time(18, 00)
from bson import ObjectId
myclient = pymongo.MongoClient("mongodb://localhost:27017/")


class StoreSearch(object):

    def __init__(self, req, follow_up_intent=""):
        self.req = req
        self.follow_up_intent = follow_up_intent.strip(" ")
        self.collection = myclient["tdm"]["stores"]
        self.parameters = self.req.get_parameters
        self.store_id = ""

    def get_response_data(self):
        floor = self.parameters["floors"] if "floors" in self.parameters else ""
        store = self.parameters["stores"] if "stores" in self.parameters else ""
        if not self.follow_up_intent:
            if store and floor:
                text = self.get_store_on_floor(store, floor)
            if store and not floor:
                data = list(self.collection.find({"store_name": store}))
                if len(data) == 1 and data[0]["is_dubai_mall"]:
                    text = "You will find %s in Dubai Mall on %s, near %s" % (
                        data[0]["store_name"], data[0]["floor"], data[0]["parking"])
                    if data[0]["is_namshi"]:
                        text = text + ". You can also find %s in Namshi.com" %(data[0]["store_name"])
                    self.store_id = data[0]["_id"]
                elif len(data) == 1 and data[0]["is_namshi"]:
                    text = "You can find %s in Namshi.com. Check it out now at Namshi.com." % (
                        data[0]["store_name"])
                    self.store_id = data[0]["_id"]
                elif data and len(data) > 1 and data[0]["is_dubai_mall"]:
                    text = "There are %s %s in The Dubai Mall. We would recommend %s on the %s" % (
                        len(data), store, data[0]["store_name"], data[0]["floor"])
                    if data[0]["is_namshi"]:
                        text = text + ". You can also find out this in Namshi.com"
                    self.store_id = data[0]["_id"]
                elif data and len(data) > 1 and data[0]["is_namshi"]:
                    text = "You can find %s in Namshi.com. Check it out now at Namshi.com." % (
                        data[0]["store_name"])
                    self.store_id = data[0]["_id"]
                else:
                    text = "We don't have this for the moment." \
                           "But, we'll let our team know that you've inquired about it."
            else:
                text = "We don't have this for the moment." \
                       "But, we'll let our team know that you've inquired about it."
            res = Response(text,"ACTIONS_ON_GOOGLE",self.req.session)
            res.add_simple_response([text])
            res.add_output_context("store", 4, {"store_name": store, "floor": floor, "store_id": str(self.store_id)})
        else:
            res = self.get_response_for_followup()

        return res.response_json

    def get_store_on_floor(self, store, floor):
        data = list(self.collection.find({"store_name": store, "floor": floor}))

        if data:
            text = "Yes, there is a %s store is in Dubai Mall on %s, near %s" % (
                data[0]["store_name"], data[0]["floor"], data[0]["parking"])
            self.store_id = data[0]["_id"]
        else:
            data = list(self.collection.find({"store_name": store}))
            if data:
                text = "Sorry there isn't a %s store on the %s , but we do have one on %s near %s" % (
                    data[0]["store_name"], floor, data[0]["floor"], data[0]["parking"])
                self.store_id = data[0]["_id"]
            else:
                text = "No, it is not in Dubai Mall, but it is available on Noon!"
        return text

    def get_response_for_followup(self):
        parameters = self.req.get_context_parameters("store")
        store_id = parameters["store_id"] if "store_id" in parameters else ""
        store_name = parameters["store_name"] if "store_name" in parameters else ""
        if self.follow_up_intent == "openclose":
            res = self.get_response_for_store_open_close_followup(store_id,store_name)
        elif self.follow_up_intent == "floor":
            res = self.get_response_for_store_floor_followup(store_name)
        elif self.follow_up_intent == "tags":
            res = self.get_response_for_store_tag_followup(store_id,store_name)
        elif self.follow_up_intent == "categories":
            res = self.get_response_for_store_category_followup(store_id,store_name)
        return res

    def get_response_for_store_open_close_followup(self,store_id,store_name):
        openclose = self.req.get_parameters["openclose"]
        day = self.req.get_parameters["day"]
        weekday = self.req.get_parameters["weekday"]
        data = self.collection.find_one({"_id":ObjectId(store_id)})
        if weekday:
            text = "%s is %s on %s" % (store_name, openclose,weekday)
        elif day:
            if day == "today":
                text = "%s is %s now" % (store_name, openclose)
            else:
                text = "%s will %s %s from %s to %s" % (store_name, openclose,day,data["opening_time"],data["closing_time"])
        else:
            if openclose == "open":
                text = "%s is open till midnight" % (store_name)
            else:
                text = "%s closes at midnight" % (store_name)

        res = Response(text, "ACTIONS_ON_GOOGLE")
        res.add_simple_response([text])
        return res

    def get_response_for_store_floor_followup(self,store_name):
        floor = self.req.get_parameters["floors"]
        data = list(self.collection.find({"store_name": store_name, "floor": floor}))

        if data:
            text = "Yes will find %s in Dubai Mall on %s, near %s" % (
                data[0]["store_name"], data[0]["floor"], data[0]["parking"])
        else:
            data = list(self.collection.find({"store_name": store_name}))
            # if data:
            text = "No, %s is not on %s, but there is one on %s near %s" % (
                data[0]["store_name"], floor, data[0]["floor"], data[0]["parking"])
            # else:
            #     text = "No, it is not in Dubai Mall, but it is available on Noon!"
        res = Response(text, "ACTIONS_ON_GOOGLE")
        res.add_simple_response([text])
        return res

    def get_response_for_store_tag_followup(self,store_id,store_name):

        tag = self.parameters["tags"] if "tags" in self.parameters else ""
        data = list(self.collection.find({"tags": {"$in": [tag]}, "_id": ObjectId(store_id)}))
        if data:
            text = "Yes %s have %s" % (store_name, tag)
        else:
            data = list(self.collection.find({"tags": {"$in": [tag]}}))
            if data and len(data) == 1:
                text = "No they don't have %s. But %s on %s near %s offers %s" % (
                    tag, data[0]["store_name"], data[0]["floor"],
                    data[0]["parking"], tag)
            elif data and len(data) == 2:
                text = "No they don't have %s. But there are %s stores in The Dubai Mall which offers %s. First is %s on %s and the second is %s on %s" % (
                    tag, len(data), tag, data[0]["store_name"], data[0]["floor"], data[1]["store_name"],
                    data[1]["floor"])
            elif data and len(data) > 2:
                text = "No they don't have %s. But there are %s stores in The Dubai Mall which offers %s. We would recommend %s on %s" % (
                    tag, len(data), tag, data[0]["store_name"], data[0]["floor"])
                self.recommended_store = data[0]
            else:
                text = "No store is available in Dubai Mall for %s, but it is available on Noon!" % (tag)
        res = Response(text, "ACTIONS_ON_GOOGLE")
        res.add_simple_response([text])
        return res

    def get_response_for_store_category_followup(self,store_id,store_name):
        category = self.parameters["categories"] if "categories" in self.parameters else ""
        data = list(self.collection.find({"tags": {"$in": [category]}, "_id": ObjectId(store_id)}))
        if not data:
            data = list(self.collection.find({"categories": {"$in": [category]}, "_id": ObjectId(store_id)}))
        if data:
            text = "Yes %s have %s" % (data[0]["store_name"], category)
        else:
            data = list(self.collection.find({"categories": {"$in": [category]}}))
            if data and len(data) == 1:
                text = "No they don't have %s. But %s on %s near %s offers %s" % (
                    category, data[0]["store_name"], data[0]["floor"],
                    data[0]["parking"], category)
                self.recommended_store = data[0]
            elif data and len(data) == 2:
                text = "No they don't have %s. But there are %s stores in The Dubai Mall which offers %s. First is %s on %s and the second is %s on %s" % (
                    category, len(data), category, data[0]["store_name"], data[0]["floor"], data[1]["store_name"],
                    data[1]["floor"])
            elif data and len(data) > 2:
                text = "No they don't have %s. But there are %s stores in The Dubai Mall which offers %s. We would recommend %s on %s" % (
                    category, len(data), category, data[0]["store_name"], data[0]["floor"])
                self.recommended_store = data[0]
            else:
                text = "No store is available in Dubai Mall for %s, but it is available on Noon!" % (category)
        res = Response(text, "ACTIONS_ON_GOOGLE")
        res.add_simple_response([text])
        return res