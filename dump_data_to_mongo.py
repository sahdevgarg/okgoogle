import pymongo
import json

# Reading an excel file using Python
import xlrd
from entities import create_entity_json_file

myclient = pymongo.MongoClient("mongodb://localhost:27017/")


class DumpData(object):
    def __init__(self):
        self.mydb = ""
        if "tdm" not in myclient.list_database_names():
            self.mydb = myclient["tdm"]
        else:
            self.mydb = myclient["tdm"]
        if "stores" not in self.mydb.list_collection_names():
            self.collection = self.mydb["stores"]
        # if "namshi_store" not in self.mydb.list_collection_names():
        #     self.mydb["namshi_store"]

        wb = xlrd.open_workbook('Store Details TDM.xlsx')
        wb = xlrd.open_workbook('Namshi Brands 2018.xlsx')
        self.sheet = wb.sheet_by_index(0)
        self.create_entity_json()
        # self.namshi_migration()
        # self.start_migration()
        # self.create_categories_json()
        # self.create_tags_json()
        # self.create_floor_json()

    def namshi_migration(self):
        store_data = []
        rows = self.sheet.nrows
        for row in range(1, rows):
            row = self.sheet.row_values(row)
            if not list(self.mydb.stores.find({"store_name":row[0]})):
                detail_data = {"store_name": row[0], "short_description":"",
                               "contact_number": "", "email": "", "categories": "",
                               "tags": "",
                               "floor": "", "parking": "",
                               "opening_time":"",
                               "closing_time":"",
                               "is_dubai_mall":False,"is_namshi":True}
                store_data.append(detail_data)
            # print ("sss",row[0])
            else:
                print ("yes")
                self.mydb.stores.update_many(
                    {"store_name":row[0]},
                    {
                        "$set": {
                            "is_namshi": True,
                        }
                    }
                )
        self.mydb.stores.insert_many(store_data)

    def start_migration(self):
        store_data = []
        rows = self.sheet.nrows
        print ("aaa",rows)
        for row in range(1, rows):
            row = self.sheet.row_values(row)
            categories = row[4].split(",")
            cat_data = []
            for category in categories:
                cat_data.append(category.strip())
            tags = row[5].split(",")
            tag_data = []
            for tag in tags:
                tag_data.append(tag.strip())
            detail_data = {"store_name": row[0], "short_description": row[1],
                           "contact_number": row[2], "email": row[3], "categories": cat_data,
                           "tags": tag_data,
                           "floor": row[6], "parking": row[7],
                           "opening_time":"10 A.M",
                           "closing_time":"midnight",
                           "is_dubai_mall":True,"is_namshi":False}
            store_data.append(detail_data)
            print ("sss",row[0])
        self.mydb.stores.insert_many(store_data)

    def create_entity_json(self):
        stores = []
        categories = []
        tags = []
        floors = []
        stores_data = list(self.mydb.stores.find())
        for store in stores_data:
            stores.append(store["store_name"])
            if store["categories"]:
                categories.extend(store["categories"])
            if store["tags"]:
                tags.extend(store["tags"])
            if store["floor"]:
                floors.append(store["floor"])
        stores = list(set(stores))
        categories = list(set(categories))
        tags = list(set(tags))
        floors = list(set(floors))
        store_data = [{"value": store, "synonyms": [store]} for store in stores]
        category_data = [{"value": category, "synonyms": [category]} for category in categories]
        tag_data = [{"value": tag, "synonyms": [tag]} for tag in tags]
        floor_data = [{"value": floor, "synonyms": [floor]} for floor in floors]
        create_entity_json_file.CreateEntityFile(store_data, "stores", True)
        create_entity_json_file.CreateEntityFile(category_data, "categories", True)
        create_entity_json_file.CreateEntityFile(tag_data, "tags", True)
        create_entity_json_file.CreateEntityFile(floor_data, "floors", True)

    def create_categories_json(self):
        rows = self.sheet.nrows
        categories = []
        for row in range(1, rows):
            row = self.sheet.row_values(row)
            for category in row[4].split(","):
                if category != "":
                    categories.append(category.strip(" "))
        categories = list(set(categories))
        print (len(categories))
        category_data = [{"value": category, "synonyms": [category]} for category in categories]
        create_entity_json_file.CreateEntityFile(category_data, "categories", True)

    def create_tags_json(self):
        rows = self.sheet.nrows
        tags = []
        for row in range(1, rows):
            row = self.sheet.row_values(row)
            for tag in row[5].split(","):
                if tag != "":
                    tags.append(tag.strip(" "))
        tags = list(set(tags))
        tag_data = [{"value": tag, "synonyms": [tag]} for tag in tags]
        create_entity_json_file.CreateEntityFile(tag_data, "tags", True)

    def create_floor_json(self):
        floors = []
        rows = self.sheet.nrows
        for row in range(1, rows):
            row = self.sheet.row_values(row)
            floors.append(row[6])
        floors = list(set(floors))
        floor_data = [{"value": floor, "synonyms": [floor]} for floor in floors]
        create_entity_json_file.CreateEntityFile(floor_data, "floors", True)

    def dump_namshi_store(self):
        wb = xlrd.open_workbook('Namshi Brands 2018.xlsx')
        sheet = wb.sheet_by_index(0)
        rows = sheet.nrows
        stores = []
        for row in range(1, rows):
            row = sheet.row_values(row)
            store_name = (" ".join(row[1].split("_"))).title()
            stores.append({"store_name":store_name})
        self.mydb.namshi_store.insert_many(stores)
