SUGGESTIONS_OBJ = {
    "suggestions": [
    ]
}

SUGGESTION_OBJ = {
    "title": ""
}

BASIC_CARD_OBJ = {
    "title": "",
    "subtitle": "",
    "formattedText": "",
    "image": {
        "imageUri": "",
        "accessibilityText": "assd"
    },
    "buttons": []
}

CARD_BUTTON_OBJ = {
    "title": "",
    "openUriAction": {
        "uri": ""
    }
}

SIMPLE_RESPONSE_OBJ = {
    "simpleResponses": [
    ]
}

CONTEXT_OBJ = {
  "name": "",
  "lifespanCount": "",
  "parameters": {}
}