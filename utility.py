from datetime import datetime, time,timedelta
from conf.holiday_list import HOLIDAYS

WEEK_DAYS = (("monday",0),("tuesday",1),("wednesday",2),("thrusday",3),("friday",4),("saturday",5),("sunday",6))

def is_time_between(begin_time, end_time, check_time=None):
    # If check time is not given, default to current UTC time
    check_time = check_time or datetime.now().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else:  # crosses midnight
        return check_time >= begin_time or check_time <= end_time


def time_diff(enter, exit):
    total_seconds = (
                datetime.combine(datetime.today(), exit) - datetime.combine(datetime.today(), enter)).total_seconds()
    return int(total_seconds / 3600)


def get_date_from_day(day):
    if day == "today" or day == "now":
        return datetime.now().date()
    elif day == "tomorrow":
        return datetime.now().date() + timedelta(days=1)
    elif day == "day after tomorrow":
        return datetime.now().date() + timedelta(days=2)


def get_date_from_weekday(weekday):
    date_val = datetime.now().date()
    for weday in WEEK_DAYS:
        if weday[0] == weekday:
            weekday_val = weday[1]
            break
    while date_val.weekday() != weekday_val:
        date_val = date_val + timedelta(days=1)
    return date_val


def check_for_holiday(date):
    for holiday in HOLIDAYS:
        if holiday[0] == str(date):
            return holiday[2],holiday[1]
        else:
            return False,""


def get_holiday_date(holiday_name):
    for holiday in HOLIDAYS:
        if holiday[1] == holiday_name:
            return holiday[0]
